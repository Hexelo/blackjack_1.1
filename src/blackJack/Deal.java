package blackJack;

import java.util.ArrayList;

public class Deal extends Hand
	{
	/*
	 * Klassen Deals syfte �r att genom att anropa metoderna i Deck och f� tillg�ng till alla element
	 * i kortleken, dra kort som flyttas fr�n leken till Handen
	 */
		private ArrayList<Cards> deck = new ArrayList<Cards>();
		
		public void createDeck()
		{
			Deck myDeck = new Deck();
			myDeck.createCard();
			myDeck.shuffle();
			deck = myDeck.getDeck();
			
		}
		
		/*
		 * H�r dras kort �nda tills leken tar slut f�r att sedan skapa en ny kortlek
		 */
		public Cards drawCard()
		{
			Cards card = null;
			try {
				card = deck.get((deck.size()-1));
				return card;
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("Slut p� kort! Nya kort blandas.");
				createDeck();
				card = deck.get((deck.size()-1));
				return card;
			}
			
		}
		
		public void removeCard()
		{
			deck.remove((deck.size()-1));
		}
}
