package blackJack;
import java.util.ArrayList;

import blackJack.Value;

public abstract class Cards
{
	/*
	 * Klassen Cards syfte �r att skapa korten med dess tv� v�rden (val�r "value", v�rde "cardValue")
	 * och l�gga in dem i en array-lista. Klassen �r abstrakt.
	 */
	private int cardValue;
	private Value value;
	private static ArrayList<Value> valueList = new ArrayList<Value>();

	/*
	 * Min Arraylista valueList fylls med val�rer
	 */
	
	public Cards()
	{
		valueList.add(Value.ACE);
		valueList.add(Value.TWO);
		valueList.add(Value.THREE);
		valueList.add(Value.FOUR);
		valueList.add(Value.FIVE);
		valueList.add(Value.SIX);
		valueList.add(Value.SEVEN);
		valueList.add(Value.EIGHT);
		valueList.add(Value.NINE);
		valueList.add(Value.TEN);
		valueList.add(Value.JACK);
		valueList.add(Value.QUEEN);
		valueList.add(Value.KING);
	}
	
	/*
	 * Val�rerna av typen Enum f�r sina respektive int-v�rden
	 */
	
	public void setCardValue()
	{
		switch(getValue())
		{
			case ACE:
				this.cardValue = 11;
				break;
			case TWO:
				this.cardValue = 2;
				break;
			case THREE:
				this.cardValue = 3;
				break;
			case FOUR:
				this.cardValue = 4;
				break;
			case FIVE:
				this.cardValue = 5;
				break;
			case SIX:
				this.cardValue = 6;
				break;
			case SEVEN:
				this.cardValue = 7;
				break;
			case EIGHT:
				this.cardValue = 8;
				break;
			case NINE:
				this.cardValue = 9;
				break;
			case TEN:
				this.cardValue = 10;
				break;
			case JACK:
				this.cardValue = 10;
				break;
			case QUEEN:
				this.cardValue = 10;
				break;
			case KING:
				this.cardValue = 10;
				break;
			default: System.out.println("Invalid card");
				return;

		}
		
	}
	
	/*
	 * Metoden setCardValue �r det f�rsta steget till att kunna omvandla �ssets v�rde 11 till v�rde 1.
	 * F�ljs upp i klassen Hand av metoden cardSum
	 */
	
	public void setCardValue(Value value)
	{
		if(value.equals(Value.ACE))
		{
			this.cardValue = 1;
		}
	}
	
	/*
	 * Getters och setters f�r de instansvariablerna
	 */
	
	public int getCardValue()
	{
		return cardValue;
	}
	

	public Value getValue() 
	{
		return value;
	}


	public void setValue(Value value) 
	{
		this.value = value;
	}
	
	public ArrayList<Value> getValueList()
	{
		return valueList;
	}
}


