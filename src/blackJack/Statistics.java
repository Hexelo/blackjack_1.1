package blackJack;

public abstract class Statistics 
{
	/*
	 * Klassen Statistics har som sin uppgift att ta emot antal givar som gjorts,
	 *j�mf�ra dem med antalet Black Jacks som innfallit
	 * och r�kna ut det procentuella utfallet f�r Black Jack (endast f�r spelarens resultat, ej dealerns)
	 */
	private double turns;
	private double bj;
	private double percent;
	
	public void noOfTurns()
	{
		turns++;
	}
	
	public void setBj()
	{
		bj++;
	}
	
	public void statisticPerent()
	{
		try 
		{
			percent = bj/turns;
			System.out.println("\nDu fick Black Jack " + bj + " g�nger p� " + turns + " antal givar");
			System.out.println("Reslutat Black Jack: " + percent*100 + "%");
		} 
		catch (ArithmeticException e) 
		{
			System.out.println("Du har f�tt en felr�kning eller resultatet 0% p� Black Jack");
		}
	}
	
	public double getBj()
	{
		return bj;
	}
}
