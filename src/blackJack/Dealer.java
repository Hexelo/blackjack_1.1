package blackJack;

public class Dealer extends Deal
{
	/*
	 * Dealern f�r sitt namn och anv�nds som en referens
	 * i klassen BlackJack. Dealen st�r f�r alla givar.
	 */
	
	private String dealerName;

	public String getDealerName() 
	{
		return dealerName;
	}

	public void setDealerName() 
	{
		dealerName = "The Dealer";
	}
}
