package blackJack;
import java.util.ArrayList;
import java.util.Collections;

public class Deck
{
	/*
	 * Klassen Decks syfte �r att skapa kortleken genom att para ihop arraylistans items med
	 * de fyra f�rgerna 
	 */
	private ArrayList<Cards> deck = new ArrayList<Cards>();
	
	
	public void addCardToList(Cards card)
	{
		deck.add(card);
	}
	
	public ArrayList<Cards> getDeck()
	{
		return deck;
	}
	
	/*
	 * I metoden createCard skapas de 52 korten med samtliga v�rden.
	 * Jag till�mpar polymorphism f�r varje av de fyra f�rgerna.
	 * Metoden anropar ocks� metoden addCardToList f�r att l�gga in
	 * varje kort i en ny arraylista.
	 */
	
	public void createCard()
	{
		Cards card = null;

		for(int i = 0; i < 13; i++)
		{
			Diamonds diamonds = new Diamonds();
			diamonds.setValue(diamonds.getValueList().get(i));
			diamonds.setCardValue();
			card = diamonds;
			addCardToList(card);
			
			Hearts hearts = new Hearts();
			hearts.setValue(hearts.getValueList().get(i));
			hearts.setCardValue();
			card = hearts;
			addCardToList(card);
	
			Spades spades = new Spades();
			spades.setValue(spades.getValueList().get(i));
			spades.setCardValue();
			card = spades;
			addCardToList(card);
			
			Clubs clubs = new Clubs();
			clubs.setValue(clubs.getValueList().get(i));
			clubs.setCardValue();
			card = clubs;
			addCardToList(card);

		}	
	}
	
		/*
		 * Metoden blandar leken och anv�nds i klassen Deals createDeck-metod
		 */
	
	public void shuffle() 
	{
        Collections.shuffle(deck);
	}
}
