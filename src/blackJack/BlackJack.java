package blackJack;

import java.util.Scanner;

public class BlackJack 
{
	/*
	 * Denna klass sk�ter spelet och genom en meny som refererar till tv�
	 * metoder kan anv�ndaren v�lja mellan simmulering och riktigt spel.
	 * H�r skapas ocks� spelets regler.
	 */
	
	Scanner input = new Scanner(System.in);
	private String answer;

	public BlackJack()
	{
		System.out.println("MENY\n\nSpela simuleringen: 1\nSpela riktiga Black Jack: 2\n");
		answer = input.next();
		
		if(answer.equals("1"))
		{
			simulation();
		}
		else if(answer.equals("2"))
		{
			play();
		}
		
	}
	/*
	 * Endaast f�rsta delen av uppgiften utf�rs, allts� simmuleringen och 
	 * nyttjar statestiken. Dealern sk�ter fortfarande givarna, men
	 * en spelare drar kort och drar alla i f�ljd automatiskt.
	 */
	public void simulation()
	{
		Player player = new Player();
		player.setName();
		Dealer dealer = new Dealer();
		dealer.setDealerName();
		dealer.createDeck();
		Cards card = null;
		
		System.out.println("\nHur m�nga givar vill du spela f�r statestiken?\n");
		int turns = input.nextInt();
		
		for(int i = 0; i < turns; i++)
		{
			player.reset();
			card = dealer.drawCard();
			System.out.println("\n" + player.getName() + player.toString(card));
			player.addCard(card);
			dealer.removeCard();
			card = dealer.drawCard();
			System.out.println(player.getName() + player.toString(card));
			player.addCard(card);
			dealer.removeCard();
			player.noOfTurns();
		}
		player.statisticPerent();
	}
	
	/*
	 * Det riktiga spelet spelas
	 */
	
	public void play()
	{
		Player player = new Player();
		player.setName();
		Dealer dealer = new Dealer();
		dealer.setDealerName();
		dealer.createDeck();
		Cards card = null;
		
		while(true)
		{ 
			player.reset();
			dealer.reset();
			
			/*
			 * F�r spelaren.
			 * F�rst dras tv� kort, sedan f�r anv�ndaren v�lja om fler kort ska dras
			 */
			
			while(true)
			{
				card = dealer.drawCard();
				System.out.println("\n" + player.getName() + player.toString(card));
				player.addCard(card);
				dealer.removeCard();
				card = dealer.drawCard();
				System.out.println(player.getName() + player.toString(card));
				player.addCard(card);
				dealer.removeCard();
				while(player.getSum() < 21)
				{
					System.out.println("Vill du dra ett till kort? Svara: y/n");
					answer = input.next();
					
					if(answer.equals("y"))
					{
						card = dealer.drawCard();
						System.out.println(player.getName() + player.toString(card));
						player.addCard(card);
						dealer.removeCard();
					}
					else
						break;
				}
				break;
			}
			/*
			 * F�r Dealern
			 */
			while(true)
			{
				/*
				 * En kort sleep-funktion anv�nds
				 */
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					
				}
				card = dealer.drawCard();
				System.out.println("\n" + dealer.getDealerName() + dealer.toString(card));
				dealer.addCard(card);
				dealer.removeCard();
				card = dealer.drawCard();
				System.out.println(dealer.getDealerName() + dealer.toString(card));
				dealer.addCard(card);
				dealer.removeCard();
				
				/*
				 * Dealern f�r inte sluta att dra kort f�rr�n denne har f�tt minst 15 po�ng
				 * i summan av v�rden.
				 */
				while(dealer.getSum() < 16)
				{
					try 
					{
						Thread.sleep(1000);
					} 
					catch (InterruptedException e) 
					{
						
					}
					card = dealer.drawCard();
					System.out.println(dealer.getDealerName() + dealer.toString(card));
					dealer.addCard(card);
					dealer.removeCard();
				}
				break;
			}
			
			if(player.getSum() > 21)
			{
				System.out.println("Dealern vann!");
			}
			
			if(dealer.getSum() > 21)
			{
				System.out.println(player.getName() + " vann!");
			}
			
			if(player.getSum() < 22 && player.getSum() > dealer.getSum())
			{
				System.out.println(player.getName() + " vann!");
			}
			if(dealer.getSum() < 22 && player.getSum() < dealer.getSum())
			{
				System.out.println("Dealern vann!");
			}
			/*
			 * Om omg�ngen skulle bli oavgjord po�ngm�ssigt vinner �nd� dealern
			 */
			if(player.getSum() == dealer.getSum())
			{
				System.out.println("Dealern vann!");
			}
			
			try 
			{
				Thread.sleep(1500);
			} 
			catch (InterruptedException e) 
			{
				
			}
				System.out.println("Vill du spela igen? Svara y/n");
				answer = input.next();
				
				if(!answer.equals("y"))
				{
					break;
				}
				
				player.noOfTurns();
		}
		
		player.statisticPerent();
	}
}
