package blackJack;

import java.util.ArrayList;

public class Hand extends Statistics
{
	/*
	 * Klassen Hands syfte �r att skapa en hand f�r spelaren och dealern som ska h�lla reda p�
	 * resultatet av de dragna korten
	 */
	
	private int sum;
	private ArrayList<Cards> cardsDrawn = new ArrayList<Cards>();
	
	/*
	 * H�r kan esset �ndra sitt v�rde fr�n 11 till 1 om summan av v�rdena p� korten
	 * som dragits �r h�gre �n 10.
	 * Ut�ver det k�nner metoden �ven av om Black Jack uppn�tts och sparar det i
	 * variabeln bj som hanteras i klassen Statistics
	 */
	
	public void CardSum(Cards card)
	{
		if(card.getValue().equals(Value.ACE) && sum>10)
		{
			card.setCardValue(Value.ACE);
		}
		
		sum += card.getCardValue();
		System.out.println("Summan av po�ngen: " + sum);
		
		if(sum == 21)
		{
			System.out.println("BLACK JACK!");
			setBj();
		}
		
	}
	
	/*
	 * Draget kort l�ggs till i arraylistan och summan f�r de kort som
	 * listan inneh�ller r�knas ihop
	 */
	
	public void addCard(Cards card)
	{
		cardsDrawn.add(card);
		CardSum(card);
		
	}
	
	public String toString(Cards card)
	{
		
		return " har dragit " + card.getValue() + " of " + card.getClass().getSimpleName();
	}
	
	/*�
	 * Efter att det sista kortet delats i draget �terst�lls v�rdena inf�r en ny omg�ng
	 */
	public void reset()
	{
		sum = 0;
		cardsDrawn.clear();
	}
	
	public int getSum()
	{
		return sum;
	}
}
